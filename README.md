# M&F Web Stack
The repository contains a sample application based on the M&F Web Stack.

## Server

### Prerequisites

The project will require .NET 4.7.1. DO NOT install the framwork from the internet. It has to be installed through the Visual Studio Installer.

To build and run the server, Microsoft Visual Studio 2017 is required at Version >= 15.6. During installation, the `.NET desktop development` workload and the `.NET Framework 4.7.1 development tools` have to be selected.

To run the server, the following command has be run once on an **elevated** command prompt:

```
netsh http add urlacl url=http://+:9000/ user=[Your Username]
```

### Building

Open the Visual Studio solution `source/MnF.WebStack/MnF.WebStack.sln` and build it. The build should complete without errors or warnings.

### Running

Select `TableSoccerServer` as startup project and hit start. After a couple of seconds the server will be listening for HTTP requests on port 9000.

## Client

### Prerequisites

To build and run the client, the following tools and packages have  to be installed:

- [Node.js](https://nodejs.org/en/), version >= 10.x
- [npm](https://www.npmjs.com/get-npm), version >= 6.x
- [angular-cli](https://github.com/angular/angular-cli), version >= 6.x

To check if all is installed as required, open a command prompt and type:

```
> ng --version
```

This should result something like:

```
     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/


Angular CLI: 6.0.3
Node: 10.1.0
OS: win32 x64
Angular: 6.0.2

...
```
### Install dependencies

Now open a command prompt in `source/table-soccer` and type:

```
> npm install
```

This will take a while as you're downloading the internet...


### Running

- Ensure the `TableSoccerServer` is up and running
- Open a command prompt in `source/table-soccer`
- Type `ng serve` and wait until `Compiled successfully.` appears
- Open a browser of your choice and navigate to <http://localhost:4200/>
- Enjoy!




