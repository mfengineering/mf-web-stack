@ECHO OFF
:: ----------------------------------------------------------------------------
:: InstallTemplates.cmd
:: Copyright Copyright (c) M+F Engineering AG. All rights reserved.
:: ----------------------------------------------------------------------------
:: Installs all templates into Visual Studio.
:: ----------------------------------------------------------------------------

PUSHD %~dp0

SET TARGET_DIR="%USERPROFILE%\Documents\Visual Studio 2017\Templates\ItemTemplates\Visual C#"

:: /s: Copies subdirectories except empty directories.
:: /xo: Excludes older files.
:: /xf: Don't copy this file.
ROBOCOPY . %TARGET_DIR% /s /xo /xf InstallTemplates.cmd

POPD
