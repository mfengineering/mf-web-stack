import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatProgressSpinnerModule,
} from '@angular/material';

import { FormComponent } from './form/form.component';
import { MainComponent } from './main/main.component';
import { OverviewComponent } from './overview/overview.component';
import { PlayersRoutingModule } from './players-routing.module';
import { UpdateComponent } from './update/update.component';
import { CreateComponent } from './create/create.component';

@NgModule({
  declarations: [
    FormComponent,
    MainComponent,
    OverviewComponent,
    UpdateComponent,
    CreateComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,

    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,

    PlayersRoutingModule,
  ],
  exports: [
  ]
})
export class PlayersModule { }
