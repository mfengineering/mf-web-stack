import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Player } from './player';
import { delayIfDebug } from '../rx-operator';

const url = 'http://localhost:9000/api/v1/player';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  constructor(private http: HttpClient) {}

  getAll(): Observable<Player[]> {
    return this.http.get<Player[]>(url);
  }

  getById(id: number): Observable<Player> {
    return this.http.get<Player>(`${url}/${id}`).pipe(
      delayIfDebug(500)
    );
  }

  update(player: Player): Observable<Player> {
    return this.http.put<Player>(`${url}/${player.id}`, player).pipe(
      delayIfDebug(500)
    );
  }

  create(player: Player): Observable<Player> {
    player.id = 0;
    return this.http.post<Player>(url, player).pipe(
      delayIfDebug(500)
    );
  }

  delete(player: Player): Observable<void> {
    return this.http.delete<void>(`${url}/${player.id}`).pipe(
      delayIfDebug(500)
    );
  }
}
