import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateComponent } from './create/create.component';
import { MainComponent } from './main/main.component';
import { OverviewComponent } from './overview/overview.component';
import { UpdateComponent } from './update/update.component';

const playersRoutes: Routes = [
  {
    path: 'players',
    component: MainComponent,
    children: [
      { path: '', component: OverviewComponent },
      { path: 'update/:id', component: UpdateComponent },
      { path: 'create', component: CreateComponent }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(playersRoutes),
  ],
  exports: [
    RouterModule,
  ]
})
export class PlayersRoutingModule { }
