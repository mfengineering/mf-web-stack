import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { PlayersService } from '../players.service';
import { Player } from '../player';
import { map, startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-players-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  private update$ = new Subject();

  players$: Observable<Player[]>;

  constructor(private playersService: PlayersService) { }

  ngOnInit() {
    this.players$ = this.update$.pipe(
      startWith(0),
      switchMap(() => this.playersService.getAll()),
      map(ps => ps.sort((l, r) => l.lastName.localeCompare(r.lastName)))
    );
  }

  delete(player: Player) {
    this.playersService.delete(player)
      .subscribe(() => this.update$.next());
  }

}
