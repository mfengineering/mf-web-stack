import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { Player } from '../player';
import { PlayersService } from '../players.service';

@Component({
  selector: 'app-players-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  player: Player = { id: null, firstName: null, lastName: null };

  constructor(private playersService: PlayersService, private location: Location) { }

  ngOnInit() {
  }

  save() {
    this.playersService.create(this.player).subscribe(() => this.navigateBack());
  }

  navigateBack() {
    this.location.back();
  }
}
