import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { Player } from '../player';
import { PlayersService } from '../players.service';

@Component({
  selector: 'app-players-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  player$: Observable<Player>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private playersService: PlayersService,
    private location: Location
  ) { }

  ngOnInit() {
    this.player$ = this.route.paramMap.pipe(
      switchMap(p => this.playersService.getById(+p.get('id')))
    );
  }

  save(player: Player) {
    this.playersService.update(player).subscribe(() => this.navigateBack());
  }

  navigateBack() {
    this.location.back();
  }
}
