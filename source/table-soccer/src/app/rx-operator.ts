import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

import { environment } from '../environments/environment';

export const delayIfDebug = (ms: number) => <T>(source: Observable<T>) => {
  if (environment.production) {
    return source;
  }
  return source.pipe(delay(ms));
};
