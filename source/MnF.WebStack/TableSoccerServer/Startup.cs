﻿//-----------------------------------------------------------------------
// <copyright file="Startup.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer
{
    using System.Collections.Generic;
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Http.ExceptionHandling;

    using Autofac;
    using Autofac.Integration.WebApi;

    using AutoMapper;

    using MnF.WebStack.Common.WebApi;

    using Newtonsoft.Json.Serialization;

    using Owin;

    /// <summary>
    /// Defines the initialization and startup sequence of this application
    /// </summary>
    internal class Startup
    {
        private readonly IContainer autofacContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        public Startup()
        {
            this.autofacContainer = ConfigureAutofac();
        }

        /// <summary>
        /// Gets assemblies of the required components.
        /// </summary>
        public static IEnumerable<Assembly> RequiredComponents
        {
            get
            {
                yield return typeof(MnF.WebStack.TableSoccer.WebApi.PlayerController).Assembly;
            }
        }

        /// <summary>
        /// Configures the application on the specified application builder.
        /// </summary>
        /// <param name="appBuilder">The application builder.</param>
        public void Configuration(IAppBuilder appBuilder)
        {
            // This server will be accessed by clients from other domains, so we open up CORS.
            // This needs to be done before the call to .MapSignalR()!
            appBuilder.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            // As we use autofac, we have to register it first.
            // This enables having a lifetime scope per request.
            appBuilder.UseAutofacMiddleware(this.autofacContainer);

            this.ConfigureWebApi(appBuilder);
        }

        private static IContainer ConfigureAutofac()
        {
            // Register all autofac modules of the required components
            var builder = new ContainerBuilder();
            foreach (var component in RequiredComponents)
            {
                builder.RegisterAssemblyModules(component);
            }

            // Configure the auto mapper with the profiles in the required components
            // and register the configuration to autofac.
            var mapperConfig = new MapperConfiguration(cfg => cfg.AddProfiles(RequiredComponents));
            builder.RegisterInstance(mapperConfig)
                .AsImplementedInterfaces();

            // register the application specific implementations.
            builder.RegisterType<Persistence.TableSoccerRepository>()
                .AsImplementedInterfaces()
                .InstancePerRequest();

            return builder.Build();
        }

        private void ConfigureWebApi(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();

            // Attribute routing.
            config.MapHttpAttributeRoutes();

            // Use autofac for the dependency resolving.
            config.DependencyResolver = new AutofacWebApiDependencyResolver(this.autofacContainer);
            appBuilder.UseAutofacWebApi(config);

            // Translate between the naming conventions fro properties.
            // In C# properties are PascalCase, but in JavaScript they are camelCase.
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Log exceptions leaking out of the controllers.
            config.Services.Add(typeof(IExceptionLogger), new NLogExceptionLogger());

            appBuilder.UseWebApi(config);
        }
    }
}
