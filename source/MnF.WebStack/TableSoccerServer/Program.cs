﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer
{
    using System;

    using Microsoft.Owin.Hosting;

    using NLog;

    using Topshelf;

    /// <summary>
    /// The main entry point of the table soccer server.
    /// </summary>
    public sealed class Program
    {
        private const string Url = "http://+:9000/";
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IDisposable app;

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns>The exit code.</returns>
        public static int Main(string[] args)
        {
            return (int)HostFactory.Run(c =>
                {
                    // Integrate NLOG with Topshelf
                    c.UseNLog();

                    // Define the service
                    c.Service<Program>(s =>
                        {
                            s.ConstructUsing(() => new Program());
                            s.WhenStarted(a => a.Start());
                            s.WhenStopped(a => a.Stop());
                        });
                });
        }

        private void Start()
        {
            this.app = WebApp.Start<Startup>(Url);
            Logger.Info("Webserver is listening on {0}", Url);
        }

        private void Stop()
        {
            this.app.Dispose();
        }
    }
}
