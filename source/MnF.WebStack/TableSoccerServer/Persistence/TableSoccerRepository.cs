﻿//-----------------------------------------------------------------------
// <copyright file="TableSoccerRepository.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Persistence
{
    using System.Data.Entity;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.TableSoccer.DomainModel;

    /// <summary>
    /// The database-based repository for table soccer entities.
    /// </summary>
    internal sealed class TableSoccerRepository : RepositoryBaseMixin<DatabaseContext>, IRepository<Match>, IRepository<Player>, IRepository<Set>
    {
        /// <summary>
        /// Gets the entities.
        /// </summary>
        IDbSet<Match> IRepository<Match>.Entities => this.Context.Matches;

        /// <summary>
        /// Gets the entities.
        /// </summary>
        IDbSet<Player> IRepository<Player>.Entities => this.Context.Players;

        /// <summary>
        /// Gets the entities.
        /// </summary>
        IDbSet<Set> IRepository<Set>.Entities => this.Context.Sets;
    }
}
