﻿//-----------------------------------------------------------------------
// <copyright file="IRepository.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.Common.DataAccess
{
    using System.Data.Entity;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Generic repository providing access to data entities.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Gets the entities.
        /// </summary>
        IDbSet<TEntity> Entities { get; }

        /// <summary>
        /// Asynchronously saves all changes.
        /// </summary>
        /// <returns>The number of objects written.</returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// Asynchronously saves all changes.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>
        /// The number of objects written.
        /// </returns>
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
