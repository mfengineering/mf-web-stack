﻿//-----------------------------------------------------------------------
// <copyright file="NLogExceptionLogger.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.Common.WebApi
{
    using System.Web.Http.ExceptionHandling;

    using NLog;

    /// <summary>
    /// Logs unhandled exceptions on WebApi requests to NLog.
    /// </summary>
    public sealed class NLogExceptionLogger : ExceptionLogger
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Logs an unhandled exception.
        /// </summary>
        /// <param name="context">The exception logger context.</param>
        public override void Log(ExceptionLoggerContext context)
        {
            Logger.Error(context.Exception);
        }
    }
}
