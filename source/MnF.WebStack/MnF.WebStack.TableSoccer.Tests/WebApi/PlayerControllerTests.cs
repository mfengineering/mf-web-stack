﻿//-----------------------------------------------------------------------
// <copyright file="PlayerControllerTests.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.WebApi
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http.Results;

    using Autofac.Extras.Moq;

    using FluentAssertions;

    using MnF.WebStack.TableSoccer.Business;
    using MnF.WebStack.TableSoccer.ResourceModel;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// Unit tests for the <see cref="PlayerController"/> class.
    /// </summary>
    internal sealed class PlayerControllerTests
    {
        private AutoMock mock;

        private static IEnumerable<Player> TestPlayers
        {
            get
            {
                var id = 1;
                yield return new Player { Id = id++, FirstName = "Alain", LastName = "Berset", };
                yield return new Player { Id = id++, FirstName = "Ueli", LastName = "Maurer", };
                yield return new Player { Id = id++, FirstName = "Doris", LastName = "Leuthard", };
                yield return new Player { Id = id++, FirstName = "Simonetta", LastName = "Sommaruga", };
                yield return new Player { Id = id++, FirstName = "Johann", LastName = "Schneider-Ammann", };
                yield return new Player { Id = id++, FirstName = "Guy", LastName = "Parmelin", };
                yield return new Player { Id = id, FirstName = "Ignazio", LastName = "Cassis", };
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.mock = AutoMock.GetLoose();

            // provide a player service serving the test players
            this.mock.Mock<IPlayerService>().Setup(s => s.GetAll())
                .Returns(Task.FromResult(TestPlayers));

            this.mock.Mock<IPlayerService>().Setup(s => s.GetById(It.IsAny<int>()))
                .Returns((int i) => Task.FromResult(TestPlayers.FirstOrDefault(p => p.Id == i)));
        }

        [TearDown]
        public void TearDown()
        {
            this.mock.Dispose();
        }

        [Test]
        public async Task GetAllSuccess()
        {
            var sut = this.mock.Create<PlayerController>();
            (await sut.GetAll()).Should().BeEquivalentTo(TestPlayers);
        }

        [Test]
        public async Task GetByIdSuccess()
        {
            var sut = this.mock.Create<PlayerController>();
            var result = await sut.GetById(2) as OkNegotiatedContentResult<Player>;
            result.Should().NotBeNull();

            // ReSharper disable once PossibleNullReferenceException
            result.Content.Id.Should().Be(2);

            result.Content.FirstName.Should().Be("Ueli");
            result.Content.LastName.Should().Be("Maurer");
        }

        [Test]
        public async Task GetByIdNotFound()
        {
            var sut = this.mock.Create<PlayerController>();
            (await sut.GetById(4711))
                .Should().BeOfType<NotFoundResult>();
        }

        [Test]
        public async Task PostAddsToService()
        {
            var sut = this.mock.Create<PlayerController>();

            await sut.PostNew(new Player { FirstName = "Foo", LastName = "Bar" });

            this.mock.Mock<IPlayerService>()
                .Verify(s => s.Add(It.Is((Player p) => p.FirstName == "Foo" && p.LastName == "Bar")), Times.Once);
        }

        [Test]
        public async Task PostSuccessReturnsCreated()
        {
            var sut = this.mock.Create<PlayerController>();

            (await sut.PostNew(new Player { FirstName = "Foo", LastName = "Bar" }))
                .Should().BeOfType<CreatedAtRouteNegotiatedContentResult<Player>>();
        }

        [Test]
        public async Task PutUpdatesToService()
        {
            var sut = this.mock.Create<PlayerController>();

            await sut.PutUpdate(4711, new Player { FirstName = "Foo", LastName = "Bar" });

            this.mock.Mock<IPlayerService>()
                .Verify(s => s.Update(It.Is((Player p) => p.Id == 4711 && p.FirstName == "Foo" && p.LastName == "Bar")), Times.Once);
        }

        [Test]
        public async Task PutSuccessReturnsOk()
        {
            var sut = this.mock.Create<PlayerController>();

            (await sut.PutUpdate(4711, new Player { FirstName = "Foo", LastName = "Bar" }))
                .Should().BeOfType<OkNegotiatedContentResult<Player>>();
        }

        [Test]
        public async Task PutUnknownReturnsNotFound()
        {
            var sut = this.mock.Create<PlayerController>();

            this.mock.Mock<IPlayerService>().Setup(s => s.Update(It.IsAny<Player>())).Throws<KeyNotFoundException>();

            (await sut.PutUpdate(4711, new Player { FirstName = "Foo", LastName = "Bar" }))
                .Should().BeOfType<NotFoundResult>();
        }
    }
}
