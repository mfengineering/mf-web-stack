﻿//-----------------------------------------------------------------------
// <copyright file="DummyTest.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer
{
    using FluentAssertions;

    using NUnit.Framework;

    /// <summary>
    /// Just a dummy test.
    /// </summary>
    [TestFixture]
    internal sealed class DummyTest
    {
        [Test]
        public void AdvancedMaths()
        {
            (1 + 1).Should().Be(2);
        }
    }
}
