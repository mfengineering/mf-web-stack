﻿//-----------------------------------------------------------------------
// <copyright file="Player.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.ResourceModel
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Resource representing a player.
    /// </summary>
    public sealed class Player
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [Required]
        public string LastName { get; set; }
    }
}
