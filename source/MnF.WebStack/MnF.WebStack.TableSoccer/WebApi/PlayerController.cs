﻿//-----------------------------------------------------------------------
// <copyright file="PlayerController.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.WebApi
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Http;

    using MnF.WebStack.Common.WebApi;
    using MnF.WebStack.TableSoccer.Business;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// WebApi controller for <see cref="Player"/> resources.
    /// </summary>
    [RoutePrefix("api/v1/player")]
    public sealed class PlayerController : ApiController
    {
        private readonly IPlayerService playerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerController"/> class.
        /// </summary>
        /// <param name="playerService">The player service.</param>
        public PlayerController(IPlayerService playerService)
        {
            this.playerService = playerService;
        }

        /// <summary>
        /// Gets all players.
        /// </summary>
        /// <returns>The players</returns>
        [Route("")]
        public async Task<IEnumerable<Player>> GetAll()
        {
            return await this.playerService.GetAll();
        }

        /// <summary>
        /// Gets the single player with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// A content result containing the requested player or not found.
        /// </returns>
        [Route("{id:int}", Name = "GetPlayerById")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var player = await this.playerService.GetById(id);
            if (player is null)
            {
                return this.NotFound();
            }

            return this.Ok(player);
        }

        /// <summary>
        /// Posts (adds) the specified new player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>A redirection to the new player.</returns>
        [Route("")]
        [ValidateModel]
        public async Task<IHttpActionResult> PostNew(Player player)
        {
            await this.playerService.Add(player);

            return this.CreatedAtRoute("GetPlayerById", new { id = player.Id }, player);
        }

        /// <summary>
        /// Puts (updates) the specified existing player.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="player">The player.</param>
        /// <returns>A content result containing the updated player or not found.</returns>
        [Route("{id:int}")]
        [ValidateModel]
        public async Task<IHttpActionResult> PutUpdate([FromUri]int id, Player player)
        {
            try
            {
                player.Id = id;
                await this.playerService.Update(player);

                return this.Ok(player);
            }
            catch (KeyNotFoundException)
            {
                return this.NotFound();
            }
        }

        /// <summary>
        /// Deletes the single player with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// An empty success.
        /// </returns>
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            await this.playerService.Delete(id);
            return this.Ok();
        }
    }
}
