﻿//-----------------------------------------------------------------------
// <copyright file="ModelBuilderExtension.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.DomainModel
{
    using System.Data.Entity;

    /// <summary>
    /// Extension methods
    /// </summary>
    public static class ModelBuilderExtension
    {
        /// <summary>
        /// Configures the specified model builder.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public static void ConfigureTableSoccer(this DbModelBuilder modelBuilder)
        {
            modelBuilder.ConfigureMatch();
            modelBuilder.ConfigurePlayer();
            modelBuilder.ConfigureSet();
        }

        private static void ConfigureMatch(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Match>()
                .HasRequired(m => m.BluePlayerOne)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Match>()
                .HasRequired(m => m.BluePlayerTwo)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Match>()
                .HasRequired(m => m.RedPlayerOne)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Match>()
                .HasRequired(m => m.RedPlayerTwo)
                .WithMany()
                .WillCascadeOnDelete(false);
        }

        private static void ConfigurePlayer(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>().Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<Player>().Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);
        }

        private static void ConfigureSet(this DbModelBuilder modelBuilder)
        {
            // Set has a combined primary key
            modelBuilder.Entity<Set>().HasKey(s => new { s.MatchId, s.Number });

            // One-to-many relationship between match an set
            modelBuilder.Entity<Set>()
                .HasRequired(s => s.Match)
                .WithMany(m => m.Sets)
                .HasForeignKey(s => s.MatchId)
                .WillCascadeOnDelete();
        }
    }
}
