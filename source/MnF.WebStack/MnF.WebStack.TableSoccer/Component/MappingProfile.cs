﻿//-----------------------------------------------------------------------
// <copyright file="MappingProfile.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Component
{
    using AutoMapper;

    /// <summary>
    /// The <see cref="AutoMapper"/> profile if the <see cref="TableSoccer"/> component.
    /// </summary>
    public sealed class MappingProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MappingProfile"/> class.
        /// </summary>
        public MappingProfile()
        {
            this.CreateMap<DomainModel.Player, ResourceModel.Player>()
                .ReverseMap();
        }
    }
}
