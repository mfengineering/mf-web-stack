﻿//-----------------------------------------------------------------------
// <copyright file="AutofacModule.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Component
{
    using Autofac;

    /// <summary>
    /// The <see cref="Autofac"/> module if the <see cref="TableSoccer"/> component.
    /// </summary>
    public sealed class AutofacModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            RegisterBusiness(builder);
            RegisterWebApi(builder);
        }

        private static void RegisterBusiness(ContainerBuilder builder)
        {
            builder.RegisterType<Business.Detail.PlayerService>()
                .AsImplementedInterfaces()
                .InstancePerRequest();
        }

        private static void RegisterWebApi(ContainerBuilder builder)
        {
            builder.RegisterType<WebApi.PlayerController>()
                .InstancePerRequest();
        }
    }
}
