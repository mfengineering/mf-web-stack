﻿//-----------------------------------------------------------------------
// <copyright file="IPlayerService.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Manages <see cref="Player"/> resources.
    /// </summary>
    public interface IPlayerService
    {
        /// <summary>
        /// Gets all players.
        /// </summary>
        /// <returns>All players.</returns>
        Task<IEnumerable<Player>> GetAll();

        /// <summary>
        /// Gets the single player with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The player; or <c>null</c> if the specified identifier is unknown.</returns>
        Task<Player> GetById(int id);

        /// <summary>
        /// Adds the specified player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>The async task.</returns>
        /// <remarks>After successfully adding the player, its identifier is set.</remarks>
        Task Add(Player player);

        /// <summary>
        /// Updates the specified player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>The async task.</returns>
        /// <exception cref="KeyNotFoundException">Thrown when the specified player is unknown.</exception>
        Task Update(Player player);

        /// <summary>
        /// Deletes the single player with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The async task.</returns>
        /// <remarks>This method doesn't fail if no player exists for the specified id.</remarks>
        Task Delete(int id);
    }
}
