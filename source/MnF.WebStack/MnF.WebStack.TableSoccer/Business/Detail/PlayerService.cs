﻿//-----------------------------------------------------------------------
// <copyright file="PlayerService.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business.Detail
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;
    using AutoMapper.QueryableExtensions;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Manages <see cref="ResourceModel.Player"/> resources.
    /// </summary>
    internal sealed class PlayerService : IPlayerService
    {
        private readonly IRepository<DomainModel.Player> playerRepository;
        private readonly IConfigurationProvider mapperConfig;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerService" /> class.
        /// </summary>
        /// <param name="playerRepository">The player repository.</param>
        /// <param name="mapperConfig">The mapper configuration.</param>
        public PlayerService(IRepository<DomainModel.Player> playerRepository, IConfigurationProvider mapperConfig)
        {
            this.playerRepository = playerRepository;
            this.mapperConfig = mapperConfig;
        }

        /// <summary>
        /// Gets all players.
        /// </summary>
        /// <returns>
        /// All players.
        /// </returns>
        public async Task<IEnumerable<Player>> GetAll()
        {
            return await this.playerRepository.Entities
                       .Where(p => !p.IsDeleted)
                       .ProjectTo<Player>(this.mapperConfig)
                       .ToListAsync();
        }

        /// <summary>
        /// Gets the single player with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The player; or <c>null</c> if the specified identifier is unknown.
        /// </returns>
        public async Task<Player> GetById(int id)
        {
            return await this.playerRepository.Entities
                       .Where(p => !p.IsDeleted)
                       .Where(p => p.Id == id)
                       .ProjectTo<Player>(this.mapperConfig)
                       .SingleOrDefaultAsync();
        }

        /// <summary>
        /// Adds the specified player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>
        /// The async task.
        /// </returns>
        /// <remarks>
        /// After successfully adding the player, its identifier is set.
        /// </remarks>
        public async Task Add(Player player)
        {
            var domain = this.mapperConfig.CreateMapper().Map<Player, DomainModel.Player>(player);
            this.playerRepository.Entities.Add(domain);
            await this.playerRepository.SaveChangesAsync();
            player.Id = domain.Id;
        }

        /// <summary>
        /// Updates the specified player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>
        /// The async task.
        /// </returns>
        /// <exception cref="KeyNotFoundException">Thrown when the specified player is unknown.</exception>
        public async Task Update(Player player)
        {
            var domain = await this.playerRepository.Entities.SingleOrDefaultAsync(p => p.Id == player.Id);
            if (domain is null)
            {
                throw new KeyNotFoundException($"No player found with id {player.Id}");
            }

            this.mapperConfig.CreateMapper().Map(player, domain);
            await this.playerRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes the single player with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The async task.
        /// </returns>
        /// <remarks>
        /// This method doesn't fail if no player exists for the specified id.
        /// </remarks>
        public async Task Delete(int id)
        {
            var domain = await this.playerRepository.Entities.SingleOrDefaultAsync(p => p.Id == id);
            if (domain is null)
            {
                return;
            }

            domain.IsDeleted = true;
            await this.playerRepository.SaveChangesAsync();
        }
    }
}
